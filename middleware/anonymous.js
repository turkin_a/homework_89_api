const User = require('../models/User');

const anonymous = async (req, res, next) => {
  const token = req.get('Token');

  req.user = {
    name: 'Anonymous'
  };

  if (!token) {
    next();
  }

  const user = await (User.findOne({token}));

  if (!user) {
    next();
  }

  req.user = user;

  next();
};

module.exports = anonymous;
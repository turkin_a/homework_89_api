const mongoose = require('mongoose');
const config = require('./config');

const Artist = require('./models/Artist');
const Album = require('./models/Album');
const Track = require('./models/Track');
const User = require('./models/User');

mongoose.connect(config.db.url + '/' + config.db.name);

const db = mongoose.connection;

db.once('open', async () => {
  try {
    await db.dropDatabase();
  } catch (e) {
    console.log('Database were not present, skipping drop...');
  }

  const [admin, user] = await User.create({
    username: 'admin',
    password: 'admin',
    role: 'admin'
  }, {
    username: 'user',
    password: 'user'
  });

  const [Nightwish, Prodigy, Aria] = await Artist.create({
    name: 'Nightwish',
    info: 'Nightwish',
    photo: 'Nightwish.jpg',
    published: true
  }, {
    userId: user._id,
    name: 'The Prodigy',
    info: 'The Prodigy',
    photo: 'Prodigy.jpg',
    published: true
  }, {
    userId: user._id,
    name: 'Ария',
    info: 'Ария',
    photo: 'Aria.jpg'
  });

  const [Wishmaster, CenturyChild, TheFatOfTheLand, InvadersMustDie, GeroyAsfalta, Chimera] = await Album.create({
    userId: user._id,
    title: 'Wishmaster',
    artist: Nightwish._id,
    year: '2000',
    poster: 'Wishmaster.jpg',
    published: true
  }, {
    userId: user._id,
    title: 'Century child',
    artist: Nightwish._id,
    year: '2002',
    poster: 'Century_Child.jpg'
  }, {
    userId: user._id,
    title: 'The Fat of the Land',
    artist: Prodigy._id,
    year: '1997',
    poster: 'The_Fat_Of_The_Land.jpg',
    published: true
  }, {
    userId: user._id,
    title: 'Invaders must die',
    artist: Prodigy._id,
    year: '2009',
    poster: 'Invaders_Must_Die.jpg'
  }, {
    title: 'Герой асфальта',
    artist: Aria._id,
    year: '1987',
    poster: 'Geroy_Asfalta.jpg'
  }, {
    title: 'Химера',
    artist: Aria._id,
    year: '2001',
    poster: 'Chimera.jpeg'
  });

  await Track.create({
    name: 'She Is My Sin',
    album: Wishmaster._id,
    duration: 286,
    number: 1,
    published: true
  }, {
    name: 'The Kinslayer',
    album: Wishmaster._id,
    duration: 239,
    number: 2
  }, {
    name: 'Come Cover Me',
    album: Wishmaster._id,
    duration: 274,
    number: 3,
    published: true
  }, {
    name: 'Wanderlust',
    album: Wishmaster._id,
    duration: 290,
    number: 4
  }, {
    name: 'Two for Tragedy',
    album: Wishmaster._id,
    duration: 231,
    number: 5,
    published: true
  }, {
    name: 'Wishmaster',
    album: Wishmaster._id,
    duration: 263,
    number: 6
  }, {
    name: 'Bare Grace Misery',
    album: Wishmaster._id,
    duration: 221,
    number: 7
  }, {
    name: 'Crownless',
    album: Wishmaster._id,
    duration: 269,
    number: 8
  }, {
    name: 'Deep Silent Complete',
    album: Wishmaster._id,
    duration: 237,
    number: 9
  }, {
    name: 'Dead Boy\'s Poem',
    album: Wishmaster._id,
    duration: 408,
    number: 10
  }, {
    name: 'FantasMic',
    album: Wishmaster._id,
    duration: 498,
    number: 11
  }, {
    name: 'Sleepwalker',
    album: Wishmaster._id,
    duration: 176,
    number: 12
  }, {
    name: 'Bless The Child',
    album: CenturyChild._id,
    duration: 373,
    number: 1
  }, {
    userId: user._id,
    name: 'End Of All Hope',
    album: CenturyChild._id,
    duration: 234,
    number: 2
  }, {
    userId: user._id,
    name: 'Dead To The World',
    album: CenturyChild._id,
    duration: 260,
    number: 3
  }, {
    userId: user._id,
    name: 'Ever Dream',
    album: CenturyChild._id,
    duration: 283,
    number: 4
  }, {
    userId: user._id,
    name: 'Slaying The Dreamer',
    album: CenturyChild._id,
    duration: 272,
    number: 5
  }, {
    name: 'Forever Yours',
    album: CenturyChild._id,
    duration: 231,
    number: 6
  }, {
    name: 'Ocean Soul',
    album: CenturyChild._id,
    duration: 254,
    number: 7,
    published: true
  }, {
    name: 'Feel For You',
    album: CenturyChild._id,
    duration: 234,
    number: 8,
    published: true
  }, {
    name: 'The Phantom Of The Opera',
    album: CenturyChild._id,
    duration: 249,
    number: 9
  }, {
    name: 'Beauty Of The Beast',
    album: CenturyChild._id,
    duration: 622,
    number: 10
  }, {
    name: 'Smack My Bitch Up',
    album: TheFatOfTheLand._id,
    duration: 343,
    number: 1,
    published: true
  }, {
    name: 'Breathe',
    album: TheFatOfTheLand._id,
    duration: 335,
    number: 2
  }, {
    name: 'Diesel Power',
    album: TheFatOfTheLand._id,
    duration: 258,
    number: 3,
    published: true
  }, {
    name: 'Funky Shit',
    album: TheFatOfTheLand._id,
    duration: 317,
    number: 4
  }, {
    name: 'Serial Thrilla',
    album: TheFatOfTheLand._id,
    duration: 311,
    number: 5,
    published: true
  }, {
    name: 'Mindfields',
    album: TheFatOfTheLand._id,
    duration: 540,
    number: 6
  }, {
    name: 'Narayan',
    album: TheFatOfTheLand._id,
    duration: 546,
    number: 7
  }, {
    name: 'Firestarter',
    album: TheFatOfTheLand._id,
    duration: 280,
    number: 8
  }, {
    name: 'Climbatize',
    album: TheFatOfTheLand._id,
    duration: 398,
    number: 9
  }, {
    name: 'Fuel My Fire',
    album: TheFatOfTheLand._id,
    duration: 259,
    number: 10
  }, {
    name: 'Invaders Must Die',
    album: InvadersMustDie._id,
    duration: 295,
    number: 1
  }, {
    name: 'Omen',
    album: InvadersMustDie._id,
    duration: 216,
    number: 2
  }, {
    name: 'Thunder',
    album: InvadersMustDie._id,
    duration: 249,
    number: 3
  }, {
    name: 'Colours',
    album: InvadersMustDie._id,
    duration: 208,
    number: 4
  }, {
    name: 'Take Me to the Hospital',
    album: InvadersMustDie._id,
    duration: 220,
    number: 5
  }, {
    name: 'Warrior\'s Dance',
    album: InvadersMustDie._id,
    duration: 313,
    number: 6
  }, {
    name: 'Run With the Wolves',
    album: InvadersMustDie._id,
    duration: 265,
    number: 7
  }, {
    name: 'Omen (reprise)',
    album: InvadersMustDie._id,
    duration: 134,
    number: 8
  }, {
    name: 'World\'s On Fire',
    album: InvadersMustDie._id,
    duration: 290,
    number: 9
  }, {
    name: 'Piranha',
    album: InvadersMustDie._id,
    duration: 245,
    number: 10
  }, {
    name: 'Stand Up',
    album: InvadersMustDie._id,
    duration: 330,
    number: 11
  }, {
    name: 'Black Smoke',
    album: InvadersMustDie._id,
    duration: 206,
    number: 12
  }, {
    name: 'Fighter Beat',
    album: InvadersMustDie._id,
    duration: 213,
    number: 13
  }, {
    name: 'На службе силы зла',
    album: GeroyAsfalta._id,
    duration: 431,
    number: 1
  }, {
    name: 'Герой асфальта',
    album: GeroyAsfalta._id,
    duration: 314,
    number: 2
  }, {
    name: 'Мертвая зона',
    album: GeroyAsfalta._id,
    duration: 403,
    number: 3
  }, {
    name: '1100',
    album: GeroyAsfalta._id,
    duration: 295,
    number: 4
  }, {
    name: 'Улица роз',
    album: GeroyAsfalta._id,
    duration: 357,
    number: 5
  }, {
    name: 'Баллада о древнерусском воине',
    album: GeroyAsfalta._id,
    duration: 512,
    number: 6
  }, {
    name: 'Химера',
    album: Chimera._id,
    duration: 279,
    number: 1
  }, {
    name: 'Небо тебя найдет',
    album: Chimera._id,
    duration: 331,
    number: 2
  }, {
    name: 'Я не сошел с ума',
    album: Chimera._id,
    duration: 385,
    number: 3
  }, {
    name: 'Вампир',
    album: Chimera._id,
    duration: 330,
    number: 4
  }, {
    name: 'Горящая стрела',
    album: Chimera._id,
    duration: 251,
    number: 5
  }, {
    name: 'Штиль',
    album: Chimera._id,
    duration: 336,
    number: 6
  }, {
    name: 'Путь в никуда',
    album: Chimera._id,
    duration: 328,
    number: 7
  }, {
    name: 'Ворон',
    album: Chimera._id,
    duration: 345,
    number: 8
  }, {
    name: 'Осколок льда',
    album: Chimera._id,
    duration: 326,
    number: 9
  }, {
    name: 'Тебе дадут знак',
    album: Chimera._id,
    duration: 533,
    number: 10
  });

  db.close();
});
const express = require('express');
const auth = require('../middleware/auth');
const anonymous = require('../middleware/anonymous');

const Track = require('../models/Track');

const router = express.Router();

const createRouter = () => {
  router.get('/:id', anonymous, (req, res) => {
    if (req.user.name === 'Anonymous') {
      Track.find({$and: [{album: req.params.id}, {published: true}]}).sort({number: 1})
        .then(results => res.send(results))
        .catch(() => res.sendStatus(500));
    } else if (req.user.role === 'admin') {
      Track.find({album: req.params.id}).sort({number: 1})
        .then(results => res.send(results))
        .catch(() => res.sendStatus(500));
    } else {
      Track.find({$and: [{album: req.params.id}, {$or: [{published: true}, {userId: req.user._id}]}]}).sort({number: 1})
        .then(results => res.send(results))
        .catch(() => res.sendStatus(500));
    }
  });

  router.post('/', auth, (req, res) => {
    const trackData = req.body;

    const track = new Track(trackData);

    track.save()
      .then(result => res.send(result))
      .catch(error => res.status(400).send(error));
  });

  return router;
};

module.exports = createRouter;
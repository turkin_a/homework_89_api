const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');

const Album = require('../models/Album');
const Track = require('../models/Track');
const auth = require('../middleware/auth');
const anonymous = require('../middleware/anonymous');
const permit = require('../middleware/permit');

const config = require('../config');

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

const router = express.Router();

const createRouter = () => {
  router.get('/:id', anonymous, (req, res) => {
    if (req.user.name === 'Anonymous') {
      Album.find({$and: [{artist: req.params.id}, {published: true}]}).sort({number: 1})
        .then(results => res.send(results))
        .catch(() => res.status(404).send({error: 'Album is not found'}));
    } else if (req.user.role === 'admin') {
      Album.find({artist: req.params.id}).sort({number: 1})
        .then(results => res.send(results))
        .catch(() => res.status(404).send({error: 'Album is not found'}));
    } else {
      Album.find({$and: [{artist: req.params.id}, {$or: [{published: true}, {userId: req.user._id}]}]}).sort({number: 1})
        .then(results => res.send(results))
        .catch(() => res.status(404).send({error: 'Album is not found'}));
    }
  });

  router.post('/', [auth, upload.single('poster')], (req, res) => {
    const albumData = req.body;

    if (req.file) {
      albumData.poster = req.file.filename;
    } else {
      albumData.poster = null;
    }

    const album = new Album(albumData);

    album.save()
      .then(result => res.send(result))
      .catch(error => res.status(400).send(error));
  });

  router.post('/:id/publish', [auth, permit('admin')], async (req, res) => {
    const album = await Album.findById(req.params.id);

    album.published = true;

    album.save()
      .then(result => res.send(result))
      .catch(error => res.status(400).send(error));
  });

  router.delete('/:id', [auth, permit('admin')], async (req, res) => {
    await Track.remove({album: req.params.id});

    const album = await Album.findById(req.params.id);

    Album.findByIdAndRemove(req.params.id)
      .then(result => res.send(album.artist))
      .catch(error => res.status(400).send(error));
  });

  return router;
};

module.exports = createRouter;
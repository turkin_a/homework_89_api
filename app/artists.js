const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');

const Artist = require('../models/Artist');
const auth = require('../middleware/auth');
const anonymous = require('../middleware/anonymous');
const permit = require('../middleware/permit');

const config = require('../config');

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

const router = express.Router();

const createRouter = () => {
  router.get('/', anonymous, (req, res) => {
    if (req.user.role === 'admin') {
      Artist.find().sort({name: 1})
        .then(results => res.send(results))
        .catch(() => res.sendStatus(500));
    } else {
      Artist.find({$or: [{published: true}, {userId: req.user._id}]}).sort({name: 1})
        .then(results => res.send(results))
        .catch(() => res.sendStatus(500));
    }
  });

  router.post('/', [auth, upload.single('photo')], (req, res) => {
    const artistData = req.body;

    if (req.file) {
      artistData.photo = req.file.filename;
    } else {
      artistData.photo = null;
    }

    const artist = new Artist(artistData);

    artist.save()
      .then(result => res.send(result))
      .catch(error => res.status(400).send(error));
  });

  router.post('/:id/publish', [auth, permit('admin')], async (req, res) => {
    const artist = await Artist.findById(req.params.id);

    artist.published = true;

    artist.save()
      .then(result => res.send(result))
      .catch(error => res.status(400).send(error));
  });

  return router;
};

module.exports = createRouter;
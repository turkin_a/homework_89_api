const express = require('express');

const User = require('../models/User');
const Track = require('../models/Track');
const Album = require('../models/Album');
const Artist = require('../models/Artist');
const History = require('../models/History');

const router = express.Router();

const createRouter = () => {
  router.get('/', async (req, res) => {
    const token = req.get('Token');
    if (!token) res.status(401).send({error: 'Unauthorized'});

    const user = await User.findOne({token: token});
    if (!user) res.status(401).send({error: 'Unauthorized'});

    History.find({user: user._id}).sort({datetime: -1})
      .then(results => {
        res.send(results)
      })
      .catch(() => res.status(404).send({error: 'Album is not found'}));
  });

  router.post('/:id', async (req, res) => {
    const token = req.body.token;

    if (!token) res.status(401).send({error: 'Unauthorized'});

    const user = await User.findOne({token: token});

    if (!user) res.status(401).send({error: 'Unauthorized'});

    const track = await Track.findById(req.params.id);
    const album = await Album.findById(track.album);
    const artist = await Artist.findById(album.artist);

    const historyData = {
      user: user._id,
      name: track.name,
      artist: artist.name,
      datetime: new Date()
    };

    const history = new History(historyData);

    history.save()
      .then(result => {
        res.send(result);
      })
      .catch(error => {
        res.status(400).send(error);
      });
  });

  return router;
};

module.exports = createRouter;
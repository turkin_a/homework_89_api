const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ArtistSchema = new Schema({
  userId: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  },
  name: {
    type: String,
    required: true,
    unique: true
  },
  info: {
    type: String,
    required: true
  },
  photo: {
    type: String,
    required: true
  },
  published: {
    type: Boolean,
    default: false
  }
});

const Artist = mongoose.model('Artist', ArtistSchema);

module.exports = Artist;
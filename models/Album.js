const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const AlbumSchema = new Schema({
  userId: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  },
  title: {
    type: String,
    required: true
  },
  artist: {
    type: Schema.Types.ObjectId,
    ref: 'Artist',
    required: true
  },
  year: {
    type: String,
    required: true
  },
  poster: {
    type: String,
    required: true
  },
  published: {
    type: Boolean,
    default: false
  }
});

const Album = mongoose.model('Album', AlbumSchema);

module.exports = Album;
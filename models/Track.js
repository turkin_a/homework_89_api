const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const TrackSchema = new Schema({
  userId: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  },
  name: {
    type: String,
    required: true
  },
  album: {
    type: Schema.Types.ObjectId,
    ref: 'Album',
    required: true
  },
  duration: {
    type: String,
    required: true
  },
  number: {
    type: Number,
    required: true
  },
  published: {
    type: Boolean,
    default: false
  }
});

const Track = mongoose.model('Track', TrackSchema);

module.exports = Track;